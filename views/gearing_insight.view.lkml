view: gearing_insight {
  sql_table_name: MODEL.GEARING_INSIGHT ;;

  dimension: engine_rpm {
    type: number
    sql: ${TABLE}."ENGINE_RPM" ;;
  }

  dimension: gear_ratio {
    type: number
    sql: ${TABLE}."GEAR_RATIO" ;;
  }

  dimension: selected_gear {
    type: number
    sql: ${TABLE}."SELECTED_GEAR" ;;
  }

  dimension: time {
    type: number
    sql: ${TABLE}."TIME" ;;
  }

  dimension: vehicle_speed {
    type: number
    sql: ${TABLE}."VEHICLE_SPEED" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

  measure: engaged_gear {
    type: average
    sql:  ${TABLE}."SELECTED_GEAR";;
    drill_fields: []

  }
}
