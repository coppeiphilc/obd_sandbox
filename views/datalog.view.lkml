view: datalog {
  sql_table_name: MODEL.DATALOG ;;

  dimension: attribute {
    type: string
    sql: ${TABLE}."ATTRIBUTE" ;;
  }

  dimension: time {
    type: number
    sql: ${TABLE}."TIME" ;;
  }

  dimension: value {
    type: number
    sql: ${TABLE}."VALUE" ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

  measure: dcount {
    type:  count_distinct
    sql:  ${TABLE}."VALUE" ;;
    drill_fields: []

  }
}
