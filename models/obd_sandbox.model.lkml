connection: "obd_snowflake"

# include all the views
include: "/views/**/*.view"

datagroup: obd_sandbox_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: obd_sandbox_default_datagroup

explore: datalog {}

explore: gearing_insight {}
